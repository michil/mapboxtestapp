package creatif.team.mapboxtestapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import creatif.team.mapboxtestapp.models.Location;
import creatif.team.mapboxtestapp.models.Point;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMarkerClickListener {

    private ArrayList<Point> points = new ArrayList<>();
    private HashMap<String, Integer> colors = new HashMap<>();
    private HashMap<String, Float> scales = new HashMap<>();

    private Bitmap cup;


    IconFactory iconFactory;

    //UI
    private MapView mapView;
    private PopupWindow mPopupWindow;
    private RelativeLayout mainContent;

    //Popup


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Настройка Popup
        mainContent = (RelativeLayout) findViewById(R.id.main_content);
        setupPopup();

        // Парсим Points
        this.points = parseData();

        // Загружаем кружку
        cup = BitmapFactory.decodeResource(this.getResources(), R.drawable.cup);
        // Добавляем цвета
        setColors();
        // Добавляем масштабы
        setScales();

        //Установка токена
        Mapbox.getInstance(this, "pk.eyJ1IjoibWljaGlsIiwiYSI6ImNqZXA1d3dyZTBjMmoyd3F4dnlmeHo4MDAifQ.xq474aBxkyk4rH5GvMJVgg");
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        iconFactory = IconFactory.getInstance(this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    /**
     * Получение данных о маркерах
     * @return массив Point
     */
    ArrayList<Point> parseData(){
        //Объявляем массив
        ArrayList<Point> points = new ArrayList<>();


        try {
            //Открываем файл
            InputStream is = getAssets().open("map.geojson");

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            //Сериализиуем
            JSONObject object = new JSONObject(new String(buffer));

            JSONArray features = object.getJSONArray("features");

            for (int i = 0; i < features.length(); i++) {

                JSONObject pointJson = features.getJSONObject(i);

                //Парсим локацию
                double lat = pointJson.getJSONObject("geometry").getJSONArray("coordinates").getDouble(1);
                double lon = pointJson.getJSONObject("geometry").getJSONArray("coordinates").getDouble(0);

                Location location = new Location(lat, lon);

                Point point = new Point(
                        i,
                        pointJson.getString("type"),
                        pointJson.getJSONObject("properties").getString("marker-color"),
                        pointJson.getJSONObject("properties").getString("marker-size"),
                        pointJson.getJSONObject("properties").getString("name"),
                        pointJson.getJSONObject("properties").getString("color"),
                        location
                );

                //Добавляем в массив
                points.add(point);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        //Возвращаем массив
        return points;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        //Получение среднего координата
        double lat = 0.0;
        double lon = 0.0;

        //Установка маркеров
        for (Point point : points){
            lat += point.getLocation().getLat();
            lon += point.getLocation().getLon();

            // Настраиваем маркер
            MarkerOptions options = new MarkerOptions();
            options.position(new LatLng(point.getLocation().getLat(), point.getLocation().getLon()));
            options.title(point.getName());


            // Настройка иконки
            int intColor = colors.get(point.getAdditional_color());
            float scale = scales.get(point.getSize());

            Icon icon = iconFactory.fromBitmap(getCupWithColor(intColor, scale));
            options.setIcon(icon);


            // Добавляем маркер
            mapboxMap.addMarker(options);
        }

        lat = lat / points.size();
        lon = lon / points.size();

        // Фокусируем карту
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(lat, lon)) // Sets the new camera position
                .zoom(10) // Sets the zoom
                .bearing(0) // Rotate the camera
                .tilt(0) // Set the camera tilt
                .build();

        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));


        //Установка Listener за маркерами
        mapboxMap.setOnMarkerClickListener(this);

    }


    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        for (Point point : points){

            if (marker.getTitle().equals(point.getName())){
                //Отображение pop up
                showPopUp(point);
                break;
            }
        }
        //Отменяем отображение родного popup
        return true;
    }


    /**
     * Отображение PopUp
     * @param point данные о точке
     */
    void showPopUp(Point point){
        // Закрываем если он активен
        if (mPopupWindow.isShowing()){
            mPopupWindow.dismiss();
        }

        View popupView = mPopupWindow.getContentView();

        try {
            popupView.setBackgroundColor(colors.get(point.getAdditional_color()));
        } catch (NullPointerException e){
            e.printStackTrace();
        }

        TextView info_text = (TextView) popupView.findViewById(R.id.info_popup_text);
        info_text.setText(point.getName());

        mPopupWindow.showAtLocation(mainContent, Gravity.CENTER,0,0);


    }


    //Генерация иконки с цветом
    private Bitmap getCupWithColor(int dstColor, float scale) {

        int width = cup.getWidth();
        int height = cup.getHeight();


        Bitmap dstBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        int [] allpixels = new int [cup.getHeight() * cup.getWidth()];

        cup.getPixels(allpixels, 0, cup.getWidth(), 0, 0, cup.getWidth(), cup.getHeight());

        for(int i = 0; i < allpixels.length; i++)
        {
            if(allpixels[i] == Color.BLACK)
            {
                allpixels[i] = dstColor;
            }
        }

        dstBitmap.setPixels(allpixels,0,cup.getWidth(),0, 0, cup.getWidth(),cup.getHeight());

        Float newWidth = width * scale;
        Float newHeight = height * scale;

        dstBitmap = Bitmap.createScaledBitmap(dstBitmap, newWidth.intValue(), newHeight.intValue(), false);

        return dstBitmap;
    }

    void setupPopup(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.info_popup,null);

        mPopupWindow = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        if(Build.VERSION.SDK_INT>=21){
            mPopupWindow.setElevation(5.0f);
        }

        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.info_popup_close);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            }
        });
    }

    void setColors(){
        colors.put("black", Color.BLACK);
        colors.put("yellow", Color.YELLOW);
        colors.put("orange", Color.rgb(255, 165, 0));
    }

    void setScales(){
        scales.put("small", 0.5f);
        scales.put("medium", 1f);
        scales.put("large", 2f);
    }

}
