package creatif.team.mapboxtestapp.models;

/**
 * Created by dingo on 13.03.2018.
 */

public class Location {

    private double lat;
    private double lon;

    public Location(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
