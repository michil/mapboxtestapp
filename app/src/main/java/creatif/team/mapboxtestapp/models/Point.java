package creatif.team.mapboxtestapp.models;

/**
 * Created by dingo on 13.03.2018.
 */

public class Point {

    private int id;
    private String type;
    private String color;
    private String size;
    private String name;
    private String additional_color;
    private Location location;

    public Point(int id, String type, String color, String size, String name, String additional_color, Location location) {
        this.type = type;
        this.color = color;
        this.size = size;
        this.name = name;
        this.additional_color = additional_color;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public String getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public String getAdditional_color() {
        return additional_color;
    }

    public Location getLocation() {
        return location;
    }
}
